
# conflictr <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

<!-- badges: end -->

`conflictr` is an R package designed to acquire, process, and visualize
international conflict data. We currently provide support for all
Uppsala Conflict Data Program (UCDP) datasets, The Armed Conflict
Location & Event Data Project (ACLED) dataset, and the Major Episodes of
Political Violence (MEPV) dataset (under development). For more
information regarding the available functionality and use see the
`conflictr` [reference manual](https://dante-sttr.gitlab.io/conflictr/).

## Installation

You can install the released version of conflictr from
[GitLab](https://gitlab.com/dante-sttr/conflictr) with:

``` r
library(devtools)
devtools::install_gitlab("dante-sttr/conflictr", dependencies=TRUE)
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).

## Example

`conflictr` provides functions to acquire data from the UCDP and ACLED
data APIs.

``` r
library(conflictr)

ucdp.dyadic<-getUCDP(db="dyadic", version="18.1")
```

    ## Sending Request to UCDP API 
    ## Processing Page 1
    ## Processing Page 2
    ## Processing Page 3

Additional datasets and versions may be specified. In some instances
`conflictr` supports data filtering via the API calls.

``` r
ucdp.ged<-getUCDP(db="gedevents", version="18.1", location = 645)
```

    ## Sending Request to UCDP API 
    ## Processing Page 1
    ## Processing Page 2
    ## Processing Page 3
    ## Processing Page 4
    ## Processing Page 5
    ## Processing Page 6
    ## Processing Page 7
    ## Processing Page 8

`conflictr` also provides default plotting functions with basemap tile
services for spatially explicit point datasets.

``` r
plot(ucdp.ged, basemap_zoom=6) +
  ggplot2::labs(title="Conflicts in Iraq from 1989 - 2018",
                subtitle = "For Battles With at Least 25 Cumulative Fatalities")
```

![](README_files/figure-gfm/unnamed-chunk-3-1.png)<!-- -->
